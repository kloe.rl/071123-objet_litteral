
// Création de l'objet movie
const movie = {
    title: "Spirited_Away",
    yearRelease: 2001,
    director: "Hayao Miyazaki",
    mainActor: ["Rumi Hiiragi", "Miyu Irino", "Mari Natsuki"],
    favoriteMovie: true,
    movieScore: 8.6,
};

// Ajout d'une propriété à l'objet movie
movie.duration = 0

// Ajout de la méthode setDuration à l'objet movie
movie.setDuration = function(hour, minute) {
    this.duration = (hour * 60) + minute;
};

// Vérification que la propriété et la méthode ont bien été ajoutées
console.log(movie)

// Vérification que la méthode fonctionne
movie.setDuration(2, 5)
console.log(movie.duration)

